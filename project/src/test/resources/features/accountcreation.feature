Feature: create an account using api

  @unit
  Scenario Outline: This is to check successful login functionality
    When the user sends "<username>" and "<password>" from the API
    Then The user's account will be created, and a response will be provided to match the "<username>".
    Examples:
      | username | password   |
      | hdsd3d241   | qwerty@123Q|
      | hwe3534r4    | qwerty@123Q|


  @unit
  Scenario Outline: The user should be able to login using the already present credentials
    When the user enters "<username>" and "<password>" into the website
    Then the user should login and have same "<username>" on his profile page
    Examples:
      | username      | password  |
      | asdf^iarsecm | qwerty@123Q|
      | asdf         | qwerty@123Q|


  @fail
  Scenario Outline: This is to check that user does not login with invalid credentials
    When the user sends "<username>" and "<password>" from the API
    Then we cannot create the account
    Examples:
      |username||password|
    # valid username and invalid password
      |asid@gmail.com|  |1231234|
    #invalid username and valid password
      |vikas@gmail.com||qwertyQ@1|
    #invalid username and invalid password
      |()d)||1231234|


