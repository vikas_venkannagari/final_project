Feature: checking if the api and ui bring the same output

  @unit
  Scenario: This is to check if the output of API and UI is the same
    When the user requests data through api
    And through the UI
    Then the details should remain the same

