package org.epam.steps;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.epam.pages.loginpages.HomePage;
import org.epam.pages.loginpages.LoginPage;
import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class AccountCreation {

    public static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(AccountCreation.class);
    private Container container;
    private LoginPage loginPage;
    private WebDriver driver;
    private HomePage homePage;

    public AccountCreation(Container container) {
        this.container = container;
        this.driver = container.driver;
        this.loginPage = container.setLoginPage();
        this.homePage = container.setHomePage();
    }

    @When("the user sends {string} and {string} from the API")
    public void theUserSendsAndFromTheAPI(String userName, String password) {
        loginPage.createAccountThroughAPI(userName, password);
        LOGGER.info("user account created");
    }

    @Then("The user's account will be created, and a response will be provided to match the {string}.")
    public void theUserSAccountWillBeCreatedAndAResponseWillBeProvidedToMatchThe(String userName) {
        Response response = loginPage.getResponse();
        response.
                then()
                .body("username", equalTo(userName))
                .statusCode(201)
                .contentType(ContentType.JSON);

        LOGGER.info("useraccount validated");
    }


    @When("the user enters {string} and {string} into the website")
    public void theUserEntersAndIntoTheWebsite(String userName, String password) throws InterruptedException {
        loginPage.login(userName, password);
        LOGGER.info("user logged in");
    }

    @Then("the user should login and have same {string} on his profile page")
    public void theUserShouldLoginAndHaveSameOnHisProfilePage(String userName) {
        assertThat(homePage.getName(), equalTo(userName));
        LOGGER.info("user loginpage validated");
        homePage.cleanUp();
    }

    @Then("we cannot create the account")
    public void weCannotCreateTheAccount() {
        Response response = loginPage.getResponse();
        Assert.assertTrue(response.statusCode()>=400 && response.statusCode()<500 );
        LOGGER.fatal("username or password are wrong");
    }
}
