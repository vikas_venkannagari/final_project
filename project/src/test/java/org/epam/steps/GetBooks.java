package org.epam.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.epam.pages.bookpages.APIBookRetriever;
import org.epam.pages.bookpages.WebsiteBookRetriever;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import java.util.*;
import java.util.stream.IntStream;

public class GetBooks{
    public static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(GetBooks.class);

    private Container container;
    private WebsiteBookRetriever websiteBookRetriever;
    private WebDriver driver;
    private APIBookRetriever apiBookRetriever;
    public GetBooks(Container container){
        this.container = container;
        this.driver = container.driver;
        this.apiBookRetriever = container.setAPI();
        this.websiteBookRetriever = container.setBooks();
    }

    @When("the user requests data through api")
    public void theUserRequestsDataThroughApi() {
        apiBookRetriever.getBooks();
        LOGGER.info("books retreived from api");
    }
    @And("through the UI")
    public void throughTheUI() throws InterruptedException {
        websiteBookRetriever.getBooks();
        LOGGER.info("books retrieved from ui");

        }


    @Then("the details should remain the same")
    public void theDetailsShouldRemainTheSame() {
        List<Map<String, String>> bookDetailsFromAPI = apiBookRetriever.getBookDetails();
        List<Map<String, String>> bookDetailsFromWebsite = websiteBookRetriever.getBookDetails();


        Assert.assertTrue( IntStream.range(0, bookDetailsFromWebsite.size())
                .allMatch(i -> bookDetailsFromAPI.get(i).equals(bookDetailsFromWebsite.get(i))));

        LOGGER.info("validated details");
    }



}


