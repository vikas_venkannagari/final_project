package org.epam.steps;

import io.cucumber.java.After;

import io.cucumber.java.AfterAll;
import org.epam.singletondriver.DriverSingleton;
import org.openqa.selenium.WebDriver;


public class Configuration {

    private Container container;
    private WebDriver driver;
    public Configuration(Container container){

        this.container = container;
        this.driver = container.driver;
    }
    @After
    public void after(){
        driver.quit();
        DriverSingleton.setDriverNull();
    }

}
