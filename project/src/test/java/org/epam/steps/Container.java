package org.epam.steps;

import org.epam.pages.bookpages.APIBookRetriever;
import org.epam.pages.bookpages.WebsiteBookRetriever;
import org.epam.pages.loginpages.HomePage;
import org.epam.pages.loginpages.LoginPage;
import org.epam.singletondriver.DriverSingleton;
import org.openqa.selenium.WebDriver;
import org.picocontainer.DefaultPicoContainer;

public class Container {

    protected WebDriver driver;

    // use singleton design pattern here to avoid creating multiple objects
    protected LoginPage loginPage;
    protected HomePage homePage;
    protected APIBookRetriever apiBookRetriever;
    protected WebsiteBookRetriever websiteBookRetriever;
    public Container() throws Exception {
        DefaultPicoContainer container = new DefaultPicoContainer();
        container.addComponent(WebDriver.class, DriverSingleton.getDriver());
        driver = container.getComponent(WebDriver.class);
    }

    public LoginPage setLoginPage(){
        if (loginPage == null)
        loginPage =  new LoginPage(driver);
        return loginPage;
    }

    public HomePage setHomePage(){
        if (this.homePage == null)
            this.homePage = new HomePage(driver);
        return this.homePage;
    }

    public APIBookRetriever setAPI(){
        if (apiBookRetriever == null)
            this.apiBookRetriever = new APIBookRetriever();
        return apiBookRetriever;
    }
    public WebsiteBookRetriever setBooks(){
        if (this.websiteBookRetriever == null);
        this.websiteBookRetriever = new WebsiteBookRetriever(driver);
        System.out.println(websiteBookRetriever);
        return this.websiteBookRetriever;
    }
}
