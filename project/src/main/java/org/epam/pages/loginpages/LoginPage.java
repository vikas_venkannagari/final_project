package org.epam.pages.loginpages;


import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.epam.pages.Page;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;

import static io.restassured.RestAssured.given;

public class LoginPage extends Page {
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private Response response;
    @FindBy(id = "userName")
    WebElement userNameField;
    @FindBy(id = "password")
    WebElement passwordField;
    @FindBy(id = "login")
    WebElement submitButton;

    public Response createAccountThroughAPI(String userName, String password){

        UserCredentials userCredentials = new UserCredentials(userName, password);

        response = given().
              contentType(ContentType.JSON)
              .body(userCredentials)
              .when()
              .post("https://demoqa.com/Account/v1/User");

        return response;
    }
    public Response getResponse(){
        return response;
    }

    public void login(String userName, String password) throws InterruptedException {
        driver.get("https://demoqa.com/login");
        userNameField.sendKeys(userName);
        passwordField.sendKeys(password);
        WebElement button = driver.findElement(By.id("login"));
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].click();", button);
        Thread.sleep(2000);
    }

    public String getUrl(){
        return driver.getCurrentUrl();
    }

}
