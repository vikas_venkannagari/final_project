package org.epam.pages.loginpages;

import org.epam.pages.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends Page {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "submit")
    WebElement deleteAccountButton;

    @FindBy(id = "userName-value")
    WebElement userName;

    public String getName(){
        return userName.getText();
    }
    public void cleanUp(){
        System.out.println("hello");
        deleteAccountButton.click();
    }
}
