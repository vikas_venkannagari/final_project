package org.epam.pages.bookpages;

import java.util.List;
import java.util.Map;

public interface Retriever {
    public List<Map<String, String>> getBooks();
    public List<Map<String, String>> getBookDetails();

}
