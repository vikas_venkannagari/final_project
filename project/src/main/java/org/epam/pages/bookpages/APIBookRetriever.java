package org.epam.pages.bookpages;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class APIBookRetriever implements Retriever {

    private List<Map<String, String >> booksDetails;
    public List<Map<String,String>> getBooks(){
        booksDetails = given().
                when()
                .get("https://demoqa.com/BookStore/v1/Books")
                .jsonPath().getList("books");

        return booksDetails;
    }

    @Override
    public List<Map<String,String>> getBookDetails() {
        return this.booksDetails;
    }


}
