package org.epam.pages.bookpages;


import org.epam.pages.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WebsiteBookRetriever extends Page implements Retriever {
    public WebsiteBookRetriever(WebDriver driver) {
        super(driver);
    }

    private List<Map<String, String>> dataList = new ArrayList<>();

    @Override
    public List<Map<String, String>> getBooks() {
        List<Map<String, String>> dataList = new ArrayList<>();

        driver.get("https://demoqa.com/books");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));


        List<WebElement> trGroups = wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.cssSelector("div.rt-tr-group")));

        for (WebElement trGroup : trGroups) {
            List<WebElement> tdElements = trGroup.findElements(By.cssSelector("div.rt-td"));

           try{
            if (tdElements.size() > 3) {

                WebElement spanElement = wait.until(ExpectedConditions.elementToBeClickable(tdElements.get(1).findElement(By.cssSelector("span.mr-2"))));
                String title = spanElement.getAttribute("id");
                title = title.replace("see-book-", "");

                String author = tdElements.get(2).getText();
                String publisher = tdElements.get(3).getText();

                Map<String, String> dataMap = new HashMap<>();
                dataMap.put("SpanId", title);
                dataMap.put("SecondValue", author);
                dataMap.put("ThirdValue", publisher);

                dataList.add(dataMap);
            }}
           catch (NoSuchElementException e){

           }
        }
        return dataList;
    }

    @Override
    public List<Map<String, String>> getBookDetails(){
        return dataList;
    }
}



