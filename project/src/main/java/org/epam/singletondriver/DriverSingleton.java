package org.epam.singletondriver;


import org.epam.exception.BrowserNotReachableException;
import org.epam.utility.ReadFile;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.epam.constants.RegistrationProperties.*;


public class DriverSingleton {

    private static WebDriver driver;

    private DriverSingleton(){

    }

    public static WebDriver getDriver() throws Exception {
        if(null==driver){
            switch (ReadFile.readPropertiesFile(BROWSER_PATH).getProperty("browser")){
                case "chrome": {
                    driver = setUpChrome();
                    break;
                }
                case "firefox":{
                    driver = setUpFirefox();
                    break;
                }
                case "edge":{
                    driver = setUpEdge();
                    break;
                }
                default:{
                    throw new BrowserNotReachableException("Please launch the correct browser");
                }
            }
        }
        return driver;
    }

    public static void setDriverNull() {

        DriverSingleton.driver = null;
    }

    public static void closeDriver(){
        driver.quit();
        driver=null;
    }

    public static WebDriver setUpChrome(){
        return new ChromeDriver();
    }

    public static WebDriver setUpFirefox(){
        return new FirefoxDriver();
    }

    public static WebDriver setUpEdge(){
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.setHeadless(true);
        return new EdgeDriver(edgeOptions);
    }
}