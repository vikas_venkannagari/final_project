package org.epam.constants;

public class RegistrationProperties {
    private RegistrationProperties(){}
    public static final String BROWSER_PATH= "src/test/resources/browser.properties";
    public static final String BASE_URI = "https://demoqa.com/Account/v1";
    public static final String ACCOUNT_CREATED_PAGE = "https://demoqa.com/profile";

}
