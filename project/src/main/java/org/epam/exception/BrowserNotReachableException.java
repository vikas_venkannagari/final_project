package org.epam.exception;

public class BrowserNotReachableException extends RuntimeException {
    public BrowserNotReachableException(String errorMessage) {
        super(errorMessage);
    }
}
