package org.epam.runner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

import org.testng.annotations.Test;

@Test
@CucumberOptions(plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
        glue = {"org.epam.steps"}, features = {"src/test/resources/features/"}, tags = "@unit")
public class UnitTestRunner extends AbstractTestNGCucumberTests
{

}
